<?php

namespace RMS\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $fillable = [
    	'name',
    	'branch_id'
    ];

    public function branch(){
    	return $this->belongsTo('RMS\Models\Branch');
    }
}
