<?php

namespace RMS\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    //
	// use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $fillable = [
		'name'
	];


	public function branches(){
		return $this->belongsToMany('RMS\Models\Branch');
	}


}
