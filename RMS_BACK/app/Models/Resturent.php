<?php

namespace RMS\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resturent extends Model
{
    // use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'name',
    	'description'

    ];

    public function branches() {
        return $this->hasMany('RMS\Models\Branch');
    }
}
