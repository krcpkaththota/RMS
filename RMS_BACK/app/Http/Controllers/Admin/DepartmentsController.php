<?php

namespace RMS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use RMS\Models\Department;
use RMS\Http\Controllers\Controller;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $departments = Department::all();

        $params = [
            'title' => 'Departments List',
            'departments' => $departments,
        ];
        return view('admin.departments.departments_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $params = [
            'title' => 'Create Department'
        ];
        return view('admin.departments.departments_create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required'
        ]);

        $department = Department::create([
            'name' => ucfirst($request->input('name'))
        ]);

        return redirect()->route('departments.index')->with('success', "The department <strong>$department->name</strong> has successfully been created.
            ");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try{
            $department = Department::findOrFail($id);

            $params = [
                'title' => 'Delete Department',
                'department' => $department,
            ];

            return view('admin.departments.departments_delete')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try
        {
            $department = Department::findOrFail($id);

            $params = [
                'title' => 'Edit Department',
                'department' => $department
            ];

            return view('admin.departments.departments_edit')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try
        {
            $department = Department::findOrFail($id);

            $this->validate($request, [
                'name' => 'required|unique:departments,name,'.$id
            ]);

            $department->name = $request->input('name');

            $department->save();

            return redirect()->route('departments.index')->with('success', "The department <strong>$department->name</strong> has successfully been updated.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {
            $department = Department::findOrFail($id);

            $department->delete();

            return redirect()->route('departments.index')->with('success', "The department <strong>{{$department->name}}</strong> has successfully been archived.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }
}
