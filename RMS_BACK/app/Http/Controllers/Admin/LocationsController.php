<?php

namespace RMS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use RMS\Models\Location;
use RMS\Models\Branch;
use RMS\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Query\Builder;
class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $locations = Location::all();
        

        $params = [
            'title' => 'Locations List',
            'locations' => $locations,
        ];
        return view('admin.locations.locations_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $branches = Branch::all();
        $params = [
            'title' => 'Create Location',
            'branches' => $branches,

        ];
        return view('admin.locations.locations_create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // var_dump($request->input('branch_id'));      

        $location = Location::create([
            'name' => $request->input('name'),
            'branch_id' => $request->input('branch_id'),
        ]);

        return redirect()->route('locations.index')->with('success', "The Location <strong>$location->name</strong> has successfully been created.
            ");
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try{
            $location = Location::findOrFail($id);

            $params = [
                'title' => 'Delete Location',
                'location' => $location,
            ];

            return view('admin.locations.locations_delete')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try
        {
            $location = Location::findOrFail($id);
            $branches = Branch::all();

            $params = [
                'title' => 'Edit Location',
                'location' => $location,
                'branches' => $branches,
            ];

            return view('admin.locations.locations_edit')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $location = Location::findOrFail($id);



            // $this->validate($request, [
            //     'name' => 'required',
            //     'address' => 'required',
            //     'email' => 'required|unique:branches,email,'.$id,
            //     'contactNo' => 'required',
            //     'resturent_id' => 'required',
            // ]);

            $location->name = $request->input('name');
            $location->branch_id = $request->input('branch_id');

            $location->save();

            return redirect()->route('locations.index')->with('success', "The location <strong>$location->name</strong> has successfully been updated.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {
            $location = Location::findOrFail($id);

            $location->delete();

            return redirect()->route('locations.index')->with('success', "The location <strong>{{$location->name}}</strong> has successfully been deleted.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }
}
