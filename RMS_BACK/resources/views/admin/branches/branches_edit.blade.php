@extends('templates.admin.layout')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Edit Branch <a href="{{route('branches.index')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> Back </a></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form method="post" action="{{ route('branches.update', ['id' => $branch->id]) }}" data-parsley-validate class="form-horizontal form-label-left">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Branch Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" value="{{$branch->name}}" id="name" name="name" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('name'))
                                <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" value="{{$branch->address}}" id="address" name="address" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('address'))
                                <span class="help-block">{{ $errors->first('address') }}</span>
                                @endif
                            </div>
                        </div> 



                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" value="{{$branch->email}}" id="email" name="email" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('contactNo') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contactNo">Contact No <span class="required">*</span>
                            </label>
                            <div class="multi-element-control col-md-6 col-sm-6 col-xs-12">
                                <?php
                                    $contactNos = explode(",",$branch->contactNo);
                                ?>
                                @if(count($contactNos))
                                    @foreach($contactNos as $contactNo)
                                        @if($contactNo != '')
                                        <div class="multi-element entry input-group"> 
                                            <input type="tel" value="{{$contactNo}}" id="contactNo" name="contactNo[]" class="form-control col-md-7 col-xs-12">
                                            <span class="input-group-btn">
                                                <button class="btn btn-danger btn-remove" type="button">
                                                    <span class="glyphicon glyphicon-minus"></span>
                                                </button>
                                            </span>
                                            @if ($errors->has('contactNo'))
                                                <span class="help-block">{{ $errors->first('contactNo') }}</span>
                                            @endif
                                        </div>
                                        @endif
                                    @endforeach
                                @endIf
                                <div class="multi-element entry input-group">                                
                                    
                                    <input type="tel" value="{{ Request::old('contactNo') ?: '' }}" id="contactNo" name="contactNo[]" class="form-control col-md-7 col-xs-12">
                                    <span class="input-group-btn">
                                        <button class="btn btn-success btn-add" type="button">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                    @if ($errors->has('contactNo'))
                                    <span class="help-block">{{ $errors->first('contactNo') }}</span>
                                    @endif
                                </div>
                            </div>                            
                        </div>

                        

                        <div class="form-group{{ $errors->has('resturent_id') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="resturent_id">Resturent <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" value="{{$branch->resturent_id}}" id="resturent_id" name="resturent_id">
                                    @if(count($resturents))
                                        @foreach($resturents as $row)
                                            <option value="{{$row->id}}" {{$row->id==$branch->resturent->id ? 'selected="selected"' : ''}}>{{$row->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('resturent_id'))
                                <span class="help-block">{{ $errors->first('resturent_id') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('departments[]') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="departments[]">Depatments <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="panel panel-default">
                                    <!-- Default panel contents -->
                                    <div class="panel-heading">Select Available Departments</div>
                                
                                    <!-- List group -->
                                    <?php
                                        $availaleDepartmentIds = [];
                                        if(count($branch->departments)){
                                            foreach ($branch->departments as $row) {
                                                array_push($availaleDepartmentIds, $row->id) ;
                                            }
                                        }

                                    ?>
                                    <ul class="list-group">
                                        @if(count($departments))
                                            @foreach($departments as $row)
                                                <li class="list-group-item">
                                                    {{$row->name}}
                                                    <div class="material-switch pull-right">
                                                        <input id="departments{{$row->id}}"  name="departments[]" type="checkbox" value="{{$row->id}}" {{in_array($row->id, $availaleDepartmentIds) ? 'checked' : ''}}/>
                                                        <label for="departments{{$row->id}}" class="label-success"></label>
                                                    </div>
                                                </li>
                                            @endforeach
                                        @endif                                       
                                    </ul>
                                </div>
                            </div>
                        </div> 

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <input name="_method" type="hidden" value="PUT">
                                <button type="submit" class="btn btn-success">Save Branch Changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop